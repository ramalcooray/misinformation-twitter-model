extensions [csv rnd nw]

;;;;;;;;;;;;;;;;;;;;;;;;
;;; Setup Procedures ;;;
;;;;;;;;;;;;;;;;;;;;;;;;
globals
[
numqualifyingnodes
clust-coeff
clustcoeffsum
tot-links
num-neighbors
current-node
m
reciprocity-percentage
suspended
top-user-followers
top-user
moderate?
investigated-user
suspended-user-circle
threshold
check
check2
]

turtles-own [ state following followers attractiveness has-links attitude]

to setup
  clear-all


  set-default-shape turtles "person"
  ;; make the initial network of two turtles and an edge
  crt 1[
   set state "S"
  ]
  crt 1[
   set state "S"
   create-link-from turtle 0
   move-to turtle 0
   fd 1
  ]
  set suspended 0

  set investigated-user nobody
  set suspended-user-circle nobody
  set threshold false
  set check false
  set check2 false
  update-colors
  reset-ticks
end

;;;;;;;;;;;;;;;;;;;;;;;
;;; Main Procedures ;;;
;;;;;;;;;;;;;;;;;;;;;;;

to go
  ;; new edge is green, old edges are gray

  ask turtles [
    set attractiveness 120
    set following count my-in-links
    set followers count my-out-links
    set attractiveness attractiveness + followers
  ]

  make-link
  tick
  if layout? [ layout ]


  if (count turtles = 1000) [
    reset-ticks
    set-top-user-followers
    stop

  ]
end


;; used for creating a new node
to make-link
  create-turtles 1
  [
    set state "S"
    set attitude 0.5
  ]
  let i 0
  while [i < 19]
  [
   ask one-of turtles
   [
    let all-others turtles
    set all-others other turtles
    set all-others all-others with [not member? self link-neighbors]
    let new-source rnd:weighted-one-of all-others [ attractiveness]
    create-link-from new-source
   ]
   set i i + 1
  ]
  update-colors
end

;;;;;;;;;;;;;;
;;; Layout ;;;
;;;;;;;;;;;;;;

to layout
  ;; the number 3 here is arbitrary; more repetitions slows down the
  ;; model, but too few gives poor layouts
  repeat 2 [
    ;; the more turtles we have to fit into the same amount of space,
    ;; the smaller the inputs to layout-spring we'll need to use
    let factor sqrt count turtles
    ;; numbers here are arbitrarily chosen for pleasing appearance
    layout-spring turtles links (1 / 1) (7 / 1) (1 / 1)
    display  ;; for smooth animation
  ]

  let x-offset max [xcor] of turtles + min [xcor] of turtles
  let y-offset max [ycor] of turtles + min [ycor] of turtles
  ;; big jumps look funny, so only adjust a little each time
  set x-offset limit-magnitude x-offset 0.1
  set y-offset limit-magnitude y-offset 0.1
  ask turtles [ setxy (xcor - x-offset / 2) (ycor - y-offset / 2) ]
end

;; resize-nodes, change back and forth from size based on degree to a size of 1
to resize-nodes
  ifelse all? turtles [size <= 1]
  [
    ;; a node is a circle with diameter determined by
    ;; the SIZE variable; using SQRT makes the circle's
    ;; area proportional to its degree
    ask turtles [ set size sqrt count link-neighbors ]
  ]
  [
    ask turtles [ set size 1 ]
  ]
end


to get-reciprocity
  let reciprocity2 0
  ask links[
    let node1 end1
    let node2 end2
    ask node2 [
      if out-link-to node1 != nobody [
        set reciprocity2 reciprocity2 + 1
      ]
    ]

    ]
  set reciprocity-percentage reciprocity2 / (count links)

end

to show-figures
   get-reciprocity

end

to-report limit-magnitude [number limit]
  if number > limit [ report limit ]
  if number < (- limit) [ report (- limit) ]
  report number
end

to plant-attitudes
  let available-turtles turtles with [count my-out-links > 0]
  let pruned-turtles turtles
  let i 0

  ask up-to-n-of (count turtles * 0.3) available-turtles[
     set attitude random-float 0.2

    set available-turtles other available-turtles
    set pruned-turtles other pruned-turtles
 ]

  ask up-to-n-of (count turtles * 0.11) available-turtles[
     set attitude 1 - (random-float 0.2)


    set available-turtles other available-turtles
    set pruned-turtles other pruned-turtles
 ]

  ask pruned-turtles[

    if (count in-link-neighbors > 0 ) [
      set attitude mean [attitude] of in-link-neighbors

    ]
  ]


end

to make-csv
 csv:to-file "attitudes.csv" [(list attitude)] of turtles
end

;;;Spreading;;;

to plant-spreader
  let available-turtles turtles with [(state != "F") and (state != "E") and (count my-out-links > 0)]

  if popularity-of-spreader = "most-popular" [
    ask max-one-of available-turtles [count my-links]
      [
        set state "F"
        set color yellow
        set size 1.5
      ]
  ]

  if popularity-of-spreader = "least-popular" [
    ask min-one-of available-turtles [count my-links]
      [
        set state "F"
        set color yellow
        set size 1.5
      ]
  ]
  if popularity-of-spreader = "random" [
    ask one-of available-turtles
      [
        set state "F"
        set color yellow
        set size 1.5
      ]
  ]
end

to update-output
  clear-output

end

to update-colors
  ask turtles [
    if state = "B" [ set color red ]
    if state = "S" [ set color gray ]
    if state = "C" [ set color blue ]
    if state = "E" [ set color magenta ]
    if state = "D" [ set color green]
  ]
end

to update-plot
  set-current-plot "State-of-people"
  set-current-plot-pen "BELIEVERS"
  plot count turtles with [state = "B"]
  set-current-plot-pen "SUSCEPTIBLE"
  plot count turtles with [state = "S"]
   set-current-plot-pen "CORRECTORS"
  plot count turtles with [state = "C"]
  set-current-plot-pen "SUSPENDED"
  plot count turtles with [state = "D"]
end

to go-spread

  tick

  if ticks > 10 [
    if check2 = false [
      if (count turtles with [state = "B"] = 0) [
        output-print "reached 0 at"
        output-print ticks
        set check2 true
      ]
    ]
  ]

  if check = false [
    if count turtles with [state = "F"] = 0[
      set check true
      output-print "removed malicious agent at"
      output-print ticks
      set check true
    ]
  ]

  if ticks > 500 [
    stop

  ]   ;; stop condition (300 units of time)

;
;  if threshold = false [
;    if (count turtles with [state = "B"] >= 600) [
;      output-print ticks
;      set threshold true
;    ]
;  ]


  if threshold = false [
    if (count turtles with [state = "C"] > count turtles with [state = "B"]) [
      output-print "C > B at"
      output-print ticks
      set threshold true
    ]
  ]
  ask turtles with [state = "S"][
    let credibilityf 0.0
    let credibilityg 0.0
    if attitudes? = true [
      set spreadingRate attitude
    ]
    let nB count in-link-neighbors with [state = "B" or state = "F"] ; n-of neighbors Believers

    if nB > 0 [
      let mean-followers mean [followers] of in-link-neighbors with [state = "B" or state = "F"]
      set credibilityf  (mean-followers / top-user-followers)
    ]

    let nF count in-link-neighbors with [state = "C"] ; n-of neighbors Fact-checkers

    if nF > 0 [
      let mean-followers mean [followers] of in-link-neighbors with [state = "C" or state = "E"]
      set credibilityg  (mean-followers / top-user-followers)
    ]
    let _1PlusAf ( 1 + credibilityf)
    let _1PlusAg ( 1 + credibilityg)
    let _1MinusAf ( 1 - credibilityf)
    let _1MinusAg ( 1 - credibilityg)
    let denf (nB * _1PlusAf + nF * _1MinusAf)
    let deng (nB * _1PlusAg + nF * _1MinusAg)
    let f 0
    let g 0
    if denf != 0 [
      set f spreadingRate * ( nB * _1PlusAf / denf )
    ]
    if deng != 0 [
      set g spreadingRate * ( nF * _1PlusAg / deng )
    ]

    let random-val-f random-float 1
    ifelse random-val-f < f
    [ set state "B" ]
    [ if random-val-f < (f + g) [ set state "C" ]
    ]
  ]

  forgetting
  verifying
  if moderation [
    let allturtles count turtles
    let turtlesfraction allturtles * (moderators-notice / 100)
    let spreadturtles count turtles with [state = "B"]
    if spreadturtles > turtlesfraction[
      set moderate? true
    ]
    if moderate? = true[
       moderate

    ]
  ]
  update-colors
  update-plot
end

to set-top-user-followers
  set top-user-followers 0
  ask turtles[

    if ( count my-out-links > top-user-followers) [
      set top-user-followers count my-out-links
    ]
  ]
end

to forgetting
  ask turtles with [state = "B" or state = "C"][
    if random-float 1 < pforget [
      set state "S"
    ]
  ]
end

to verifying  ;; B-> F ; each agent can fact-check the hoax with a fixed probability pverify;
  ask turtles with [state = "B"][
    if random-float 1 < pVerify [
      set state "C"
    ]
  ]
end

to moderate
  ifelse (investigated-user = nobody) [
     let available-turtles turtles with [state != "D"]
     ask one-of available-turtles [
       if (state = "B" or state = "F") [
        set state  "D"
        set investigated-user self
        set suspended suspended + 1
        set suspended-user-circle link-neighbors
       ]
     ]
  ]
  [
   let target  one-of suspended-user-circle
   ifelse target = nobody [
     set investigated-user nobody
     ]
   [
   ask target[
     if (state = "B" or state = "F") [
       set state  "D"
       set suspended suspended + 1
     ]
     set suspended-user-circle other suspended-user-circle
   ]
  ]
  ]
end

to reset
  clear-plot
  set suspended 0
  set investigated-user nobody
  set suspended-user-circle nobody
  set threshold false
  set check     false
  set check2    false
  ask turtles[
    set state "S"
    set attitude 0.5
  ]
  reset-ticks
end
@#$#@#$#@
GRAPHICS-WINDOW
341
10
773
443
-1
-1
8.314
1
10
1
1
1
0
0
0
1
-25
25
-25
25
1
1
1
ticks
60.0

PLOT
1183
19
1510
185
In Degree Distribution (log-log)
log(# of nodes)
log(in degree)
0.0
0.3
0.0
0.3
true
false
"" ""
PENS
"default" 1.0 2 -16777216 true "" "if not plot? [ stop ]\nlet max-degree max [count my-in-links] of turtles\n;; for this plot, the axes are logarithmic, so we can't\n;; use \"histogram-from\"; we have to plot the points\n;; ourselves one at a time\nplot-pen-reset  ;; erase what we plotted before\n;; the way we create the network there is never a zero degree node,\n;; so start plotting at degree one\nlet degree 1\nwhile [degree <= max-degree] [\n  let matches turtles with [count my-in-links = degree]\n  if any? matches\n    [ plotxy log degree 10\n             log (count matches) 10 ]\n  set degree degree + 1\n]"

PLOT
834
195
1159
371
Out Degree Distribution
out degree
# of out nodes
1.0
10.0
0.0
10.0
true
false
"" ""
PENS
"default" 1.0 1 -16777216 true "" "if not plot? [ stop ]\nlet max-degree max [count my-out-links] of turtles\nplot-pen-reset  ;; erase what we plotted before\nset-plot-x-range 1 (max-degree + 1)  ;; + 1 to make room for the width of the last bar\nhistogram [count link-neighbors] of turtles"

BUTTON
6
61
72
94
NIL
setup
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

BUTTON
7
141
84
174
NIL
go
T
1
T
OBSERVER
NIL
NIL
NIL
NIL
0

BUTTON
6
100
91
133
go-once
go
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
0

SWITCH
187
66
333
99
plot?
plot?
0
1
-1000

SWITCH
187
100
333
133
layout?
layout?
0
1
-1000

MONITOR
1653
60
1732
105
# of nodes
count turtles
3
1
11

MONITOR
1544
61
1645
106
# of links
count links
17
1
11

PLOT
835
17
1157
187
In Degree Distribution
in degree
# of nodes
0.0
10.0
0.0
10.0
true
false
"" ""
PENS
"default" 1.0 1 -16777216 true "" "if not plot? [ stop ]\nlet max-degree max [count my-in-links] of turtles\nplot-pen-reset  ;; erase what we plotted before\nset-plot-x-range 1 (max-degree + 1)  ;; + 1 to make room for the width of the last bar\nhistogram [count link-neighbors] of turtles"

PLOT
1182
200
1512
370
Out Degree Distribution (log-log)
log(# of nodes)
log(out degree)
0.0
0.3
0.0
0.3
true
false
"" ""
PENS
"default" 1.0 2 -16777216 true "" "if not plot? [ stop ]\nlet max-degree max [count my-out-links] of turtles\n;; for this plot, the axes are logarithmic, so we can't\n;; use \"histogram-from\"; we have to plot the points\n;; ourselves one at a time\nplot-pen-reset  ;; erase what we plotted before\n;; the way we create the network there is never a zero degree node,\n;; so start plotting at degree one\nlet degree 1\nwhile [degree <= max-degree] [\n  let matches turtles with [count my-out-links = degree]\n  if any? matches\n    [ plotxy log degree 10\n             log (count matches) 10 ]\n  set degree degree + 1\n]"

MONITOR
1544
299
1689
344
average in/out degree
mean [count my-links] of turtles
2
1
11

MONITOR
1545
182
1683
227
reciprocity %
reciprocity-percentage * 100
2
1
11

MONITOR
1543
238
1686
283
clustering coefficient
mean [nw:clustering-coefficient] of turtles
3
1
11

TEXTBOX
30
434
180
453
Diffusion functions
16
0.0
1

TEXTBOX
29
473
179
491
parameters of network
12
0.0
1

TEXTBOX
14
17
164
36
Network Creation
16
0.0
1

SLIDER
29
505
201
538
spreadingRate
spreadingRate
0
1
0.5
0.01
1
NIL
HORIZONTAL

SLIDER
29
557
201
590
pForget
pForget
0
1.0
0.2
0.01
1
NIL
HORIZONTAL

TEXTBOX
372
498
522
516
plant spreaders
12
0.0
1

CHOOSER
371
527
556
572
popularity-of-spreader
popularity-of-spreader
"random" "most-popular" "least-popular"
0

BUTTON
369
590
585
623
plant spreader
plant-spreader
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

BUTTON
33
700
176
733
start spreading
go-spread
T
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

SWITCH
645
533
785
566
moderation
moderation
1
1
-1000

PLOT
912
406
1530
745
State-of-People
time steps
# of users
0.0
10.0
0.0
10.0
true
true
"set-plot-y-range 0 count turtles" ""
PENS
"SUSCEPTIBLE" 1.0 0 -5987164 true "" ""
"BELIEVERS" 1.0 0 -2674135 true "" ""
"CORRECTORS" 1.0 0 -14070903 true "" ""
"SUSPENDED" 1.0 0 -13840069 true "" ""

MONITOR
1559
577
1639
622
believers
count turtles with [state = \"B\"]
17
1
11

MONITOR
1559
522
1638
567
susceptible
count turtles with [state = \"S\"]
17
1
11

MONITOR
1558
638
1638
683
correctors
count turtles with [state = \"C\"]
17
1
11

MONITOR
370
646
433
691
spreader
count turtles with [state = \"F\"]
17
1
11

BUTTON
1545
135
1683
168
NIL
show-figures
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

SLIDER
644
576
838
609
moderators-notice
moderators-notice
0
100
30.0
1
1
NIL
HORIZONTAL

TEXTBOX
645
502
795
520
moderation parameters\n
12
0.0
1

MONITOR
644
675
753
720
suspended users
suspended
17
1
11

BUTTON
33
745
178
778
NIL
reset
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

SLIDER
30
626
202
659
pVerify
pVerify
0
0.1
0.05
0.001
1
NIL
HORIZONTAL

MONITOR
217
557
304
602
forgetting %
pForget * 100
17
1
11

MONITOR
216
625
306
670
verification %
pVerify * 100
17
1
11

MONITOR
643
731
754
776
active users
count turtles - suspended
17
1
11

MONITOR
1658
579
1798
624
clustering of believers
mean [nw:clustering-coefficient] of turtles with[state = \"B\" or state = \"C\"]
3
1
11

MONITOR
1658
639
1798
684
clustering of correctors
mean [nw:clustering-coefficient] of turtles with[state = \"B\"]
3
1
11

MONITOR
1658
522
1797
567
clustering of susceptible
mean [nw:clustering-coefficient] of turtles with[state = \"S\"]
3
1
11

MONITOR
1079
756
1219
801
average in/out degree [S]
mean [count my-links] of turtles with [state = \"S\"]
2
1
11

MONITOR
1237
756
1379
801
average in/out degree [C]
mean [count my-links] of turtles with [state = \"C\"]
2
1
11

MONITOR
1393
755
1530
800
average in/out degree [B]
mean [count my-links] of turtles with [state = \"B\"]
2
1
11

OUTPUT
1560
367
1800
501
11

BUTTON
364
743
476
776
plant attitudes
plant-attitudes\n
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

SWITCH
508
745
619
778
attitudes?
attitudes?
1
1
-1000

BUTTON
510
791
593
824
NIL
make-csv
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

@#$#@#$#@
## WHAT IS IT?

This model generates a network based on the topology of the social media site Twitter. The model addityionally simulates the process of information diffusion of a viral hoax in this Network. It shows how this misinformation is spread from user to user and can be combatted by correctly informed users.

The generated network is created by a process of "preferential linking", in which new links are formed between network members with a preferability to make a connection to the more popular existing members. This process is an elaboration on the fundamental preferential attachment model.

The process of information diffusion is a variation on the SIR (susceptible-infected-recovered) model used in epidemiology. A viral hoax in the Twitter network is modelled similarly to a virus in a population.

## HOW IT WORKS

The network generation procedure: 

The model starts with two users connected by a link. At each time step, a new user is added to the network. At the same time, a certain number of new in-links are randomly attached to users in the network, based on their attractiveness, which is an initial value given to all users plus the number of in-links the users has. This continues until the network reaches a certain size of users. The current settings on the model are chosen to most accurately simulate Twitter's network.

The misinformation spreading behaviour:

The number of malicious hoax spreaders can be placed manually. These users will attempt to spread the hoax to other users. 
Each user exists in one of three states:
 Believer: a agent is in a state where they believe the false information.
 Corrector: agent i is in a state where they don’t believe false information.
 Susceptible: agent i is in a state where they are neutral to the information.

At each time step, 3 phenomena can happen to users in the network based on a probability of the users around them:

Spreading: Every susceptible user can change the state it is in to either become a believer or a corrector depending on the states of the other agents that are its neighbors. This uses the Linear Threshold model, with the weight of the neighbours having to pass a threshold in order for the agent to change its state.

Verifying: Every believer can fact-check the misinformation themselves and become a corrector based on a fixed probability.

Forgetting: Every believer or corrector can forget the information entirely and
become neutral again based on a fixed probability pf orget.

Additionally, moderation can be toggled which at each time step looks for hoax spreaders in the network and removes them.

User attitudes can be toggled as well which assigns each user a certain attitude to the misinformation which could allow them to be more easily turned into a believer of the misinformation. The distribution of user attitudes was modelled on the distribution of Twitter user's attitudes towards vaccinations in the UK.

## HOW TO USE IT

Generating the Network:

SETUP is used to place the initial nodes in the network. 

Pressing the GO ONCE button adds one new node.  To continuously add nodes, press GO. The network's current size to stop growing is 1000 nodes.

The LAYOUT? switch controls whether or not the layout procedure is run.  This procedure attempts to move the nodes around to make the structure of the network easier to see.

The PLOT? switch turns off the plots which speeds up the model.

Spreading a hoax:

The credibility parameter affects how credibile the hoax is to users in the network. A high credibility will spread the hoax more easily.

The pForgetting parameter affects how easily users in the network forget the hoax.

plant spreader changes a user in the network to a spreading state. There must be at least one initial spreader for the hoax to spread.

popularity-of-spreader allows what sort of user will be chosen to be a spreader when one is planted.

The moderation toggle allows moderation to be enabled.

The attitudes toggle makes each user attitude affect the spread of the hoax.

start-spreading is used to allow time to pass and see the spread of the hoax over time.

## THINGS TO NOTICE

The networks that result from running this model are often called "scale-free" or "power law" networks. These are networks in which the distribution of the number of connections of each node is not a normal distribution --- instead it follows what is a called a power law distribution.  Power law distributions are different from normal distributions in that they do not have a peak at the average, and they are more likely to contain extreme values (see Albert & Barabási 2002 for a further description of the frequency and significance of scale-free networks).  Barabási and Albert originally described this mechanism for creating networks, but there are other mechanisms of creating scale-free networks and so the networks created by the mechanism implemented in this model are referred to as Barabási scale-free networks.

You can see the degree distribution of the network in this model by looking at the plots. The top plot is a histogram of the degree of each node.  The bottom plot shows the same data, but both axes are on a logarithmic scale.  When degree distribution follows a power law, it appears as a straight line on the log-log plot.  One simple way to think about power laws is that if there is one node with a degree distribution of 1000, then there will be ten nodes with a degree distribution of 100, and 100 nodes with a degree distribution of 10.


## CREDITS AND REFERENCES

The network model is based on:
S. Dorogovtsev, J. F. Mendes, and A. Samukhin. Structure of growing networks with preferential linking. Physical Review Letters, 85:4633–6, 11 2000.

The spreading model is based on:
M. Tambuscio, G. Ruffo, A. Flammini, and F. Menczer. Fact-checking effect on viral hoaxes: A model of misinformation spread in social networks. 05 2015

<!-- 2005 -->
@#$#@#$#@
default
true
0
Polygon -7500403 true true 150 5 40 250 150 205 260 250

airplane
true
0
Polygon -7500403 true true 150 0 135 15 120 60 120 105 15 165 15 195 120 180 135 240 105 270 120 285 150 270 180 285 210 270 165 240 180 180 285 195 285 165 180 105 180 60 165 15

arrow
true
0
Polygon -7500403 true true 150 0 0 150 105 150 105 293 195 293 195 150 300 150

box
false
0
Polygon -7500403 true true 150 285 285 225 285 75 150 135
Polygon -7500403 true true 150 135 15 75 150 15 285 75
Polygon -7500403 true true 15 75 15 225 150 285 150 135
Line -16777216 false 150 285 150 135
Line -16777216 false 150 135 15 75
Line -16777216 false 150 135 285 75

bug
true
0
Circle -7500403 true true 96 182 108
Circle -7500403 true true 110 127 80
Circle -7500403 true true 110 75 80
Line -7500403 true 150 100 80 30
Line -7500403 true 150 100 220 30

butterfly
true
0
Polygon -7500403 true true 150 165 209 199 225 225 225 255 195 270 165 255 150 240
Polygon -7500403 true true 150 165 89 198 75 225 75 255 105 270 135 255 150 240
Polygon -7500403 true true 139 148 100 105 55 90 25 90 10 105 10 135 25 180 40 195 85 194 139 163
Polygon -7500403 true true 162 150 200 105 245 90 275 90 290 105 290 135 275 180 260 195 215 195 162 165
Polygon -16777216 true false 150 255 135 225 120 150 135 120 150 105 165 120 180 150 165 225
Circle -16777216 true false 135 90 30
Line -16777216 false 150 105 195 60
Line -16777216 false 150 105 105 60

car
false
0
Polygon -7500403 true true 300 180 279 164 261 144 240 135 226 132 213 106 203 84 185 63 159 50 135 50 75 60 0 150 0 165 0 225 300 225 300 180
Circle -16777216 true false 180 180 90
Circle -16777216 true false 30 180 90
Polygon -16777216 true false 162 80 132 78 134 135 209 135 194 105 189 96 180 89
Circle -7500403 true true 47 195 58
Circle -7500403 true true 195 195 58

circle
false
0
Circle -7500403 true true 0 0 300

circle 2
false
0
Circle -7500403 true true 0 0 300
Circle -16777216 true false 30 30 240

cow
false
0
Polygon -7500403 true true 200 193 197 249 179 249 177 196 166 187 140 189 93 191 78 179 72 211 49 209 48 181 37 149 25 120 25 89 45 72 103 84 179 75 198 76 252 64 272 81 293 103 285 121 255 121 242 118 224 167
Polygon -7500403 true true 73 210 86 251 62 249 48 208
Polygon -7500403 true true 25 114 16 195 9 204 23 213 25 200 39 123

cylinder
false
0
Circle -7500403 true true 0 0 300

dot
false
0
Circle -7500403 true true 90 90 120

face happy
false
0
Circle -7500403 true true 8 8 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Polygon -16777216 true false 150 255 90 239 62 213 47 191 67 179 90 203 109 218 150 225 192 218 210 203 227 181 251 194 236 217 212 240

face neutral
false
0
Circle -7500403 true true 8 7 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Rectangle -16777216 true false 60 195 240 225

face sad
false
0
Circle -7500403 true true 8 8 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Polygon -16777216 true false 150 168 90 184 62 210 47 232 67 244 90 220 109 205 150 198 192 205 210 220 227 242 251 229 236 206 212 183

fish
false
0
Polygon -1 true false 44 131 21 87 15 86 0 120 15 150 0 180 13 214 20 212 45 166
Polygon -1 true false 135 195 119 235 95 218 76 210 46 204 60 165
Polygon -1 true false 75 45 83 77 71 103 86 114 166 78 135 60
Polygon -7500403 true true 30 136 151 77 226 81 280 119 292 146 292 160 287 170 270 195 195 210 151 212 30 166
Circle -16777216 true false 215 106 30

flag
false
0
Rectangle -7500403 true true 60 15 75 300
Polygon -7500403 true true 90 150 270 90 90 30
Line -7500403 true 75 135 90 135
Line -7500403 true 75 45 90 45

flower
false
0
Polygon -10899396 true false 135 120 165 165 180 210 180 240 150 300 165 300 195 240 195 195 165 135
Circle -7500403 true true 85 132 38
Circle -7500403 true true 130 147 38
Circle -7500403 true true 192 85 38
Circle -7500403 true true 85 40 38
Circle -7500403 true true 177 40 38
Circle -7500403 true true 177 132 38
Circle -7500403 true true 70 85 38
Circle -7500403 true true 130 25 38
Circle -7500403 true true 96 51 108
Circle -16777216 true false 113 68 74
Polygon -10899396 true false 189 233 219 188 249 173 279 188 234 218
Polygon -10899396 true false 180 255 150 210 105 210 75 240 135 240

house
false
0
Rectangle -7500403 true true 45 120 255 285
Rectangle -16777216 true false 120 210 180 285
Polygon -7500403 true true 15 120 150 15 285 120
Line -16777216 false 30 120 270 120

leaf
false
0
Polygon -7500403 true true 150 210 135 195 120 210 60 210 30 195 60 180 60 165 15 135 30 120 15 105 40 104 45 90 60 90 90 105 105 120 120 120 105 60 120 60 135 30 150 15 165 30 180 60 195 60 180 120 195 120 210 105 240 90 255 90 263 104 285 105 270 120 285 135 240 165 240 180 270 195 240 210 180 210 165 195
Polygon -7500403 true true 135 195 135 240 120 255 105 255 105 285 135 285 165 240 165 195

line
true
0
Line -7500403 true 150 0 150 300

line half
true
0
Line -7500403 true 150 0 150 150

pentagon
false
0
Polygon -7500403 true true 150 15 15 120 60 285 240 285 285 120

person
false
0
Circle -7500403 true true 110 5 80
Polygon -7500403 true true 105 90 120 195 90 285 105 300 135 300 150 225 165 300 195 300 210 285 180 195 195 90
Rectangle -7500403 true true 127 79 172 94
Polygon -7500403 true true 195 90 240 150 225 180 165 105
Polygon -7500403 true true 105 90 60 150 75 180 135 105

plant
false
0
Rectangle -7500403 true true 135 90 165 300
Polygon -7500403 true true 135 255 90 210 45 195 75 255 135 285
Polygon -7500403 true true 165 255 210 210 255 195 225 255 165 285
Polygon -7500403 true true 135 180 90 135 45 120 75 180 135 210
Polygon -7500403 true true 165 180 165 210 225 180 255 120 210 135
Polygon -7500403 true true 135 105 90 60 45 45 75 105 135 135
Polygon -7500403 true true 165 105 165 135 225 105 255 45 210 60
Polygon -7500403 true true 135 90 120 45 150 15 180 45 165 90

square
false
0
Rectangle -7500403 true true 30 30 270 270

square 2
false
0
Rectangle -7500403 true true 30 30 270 270
Rectangle -16777216 true false 60 60 240 240

star
false
0
Polygon -7500403 true true 151 1 185 108 298 108 207 175 242 282 151 216 59 282 94 175 3 108 116 108

target
false
0
Circle -7500403 true true 0 0 300
Circle -16777216 true false 30 30 240
Circle -7500403 true true 60 60 180
Circle -16777216 true false 90 90 120
Circle -7500403 true true 120 120 60

tree
false
0
Circle -7500403 true true 118 3 94
Rectangle -6459832 true false 120 195 180 300
Circle -7500403 true true 65 21 108
Circle -7500403 true true 116 41 127
Circle -7500403 true true 45 90 120
Circle -7500403 true true 104 74 152

triangle
false
0
Polygon -7500403 true true 150 30 15 255 285 255

triangle 2
false
0
Polygon -7500403 true true 150 30 15 255 285 255
Polygon -16777216 true false 151 99 225 223 75 224

truck
false
0
Rectangle -7500403 true true 4 45 195 187
Polygon -7500403 true true 296 193 296 150 259 134 244 104 208 104 207 194
Rectangle -1 true false 195 60 195 105
Polygon -16777216 true false 238 112 252 141 219 141 218 112
Circle -16777216 true false 234 174 42
Rectangle -7500403 true true 181 185 214 194
Circle -16777216 true false 144 174 42
Circle -16777216 true false 24 174 42
Circle -7500403 false true 24 174 42
Circle -7500403 false true 144 174 42
Circle -7500403 false true 234 174 42

turtle
true
0
Polygon -10899396 true false 215 204 240 233 246 254 228 266 215 252 193 210
Polygon -10899396 true false 195 90 225 75 245 75 260 89 269 108 261 124 240 105 225 105 210 105
Polygon -10899396 true false 105 90 75 75 55 75 40 89 31 108 39 124 60 105 75 105 90 105
Polygon -10899396 true false 132 85 134 64 107 51 108 17 150 2 192 18 192 52 169 65 172 87
Polygon -10899396 true false 85 204 60 233 54 254 72 266 85 252 107 210
Polygon -7500403 true true 119 75 179 75 209 101 224 135 220 225 175 261 128 261 81 224 74 135 88 99

wheel
false
0
Circle -7500403 true true 3 3 294
Circle -16777216 true false 30 30 240
Line -7500403 true 150 285 150 15
Line -7500403 true 15 150 285 150
Circle -7500403 true true 120 120 60
Line -7500403 true 216 40 79 269
Line -7500403 true 40 84 269 221
Line -7500403 true 40 216 269 79
Line -7500403 true 84 40 221 269

x
false
0
Polygon -7500403 true true 270 75 225 30 30 225 75 270
Polygon -7500403 true true 30 75 75 30 270 225 225 270
@#$#@#$#@
NetLogo 6.2.0
@#$#@#$#@
set layout? false
set plot? false
setup repeat 300 [ go ]
repeat 100 [ layout ]
@#$#@#$#@
@#$#@#$#@
@#$#@#$#@
@#$#@#$#@
default
0.0
-0.2 0 0.0 1.0
0.0 1 1.0 0.0
0.2 0 0.0 1.0
link direction
true
0
Line -7500403 true 150 150 90 180
Line -7500403 true 150 150 210 180
@#$#@#$#@
0
@#$#@#$#@
