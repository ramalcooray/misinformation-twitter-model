## WHAT IS IT?

This model generates a network based on the topology of the social media site Twitter. The model addityionally simulates the process of information diffusion of a viral hoax in this Network. It shows how this misinformation is spread from user to user and can be combatted by correctly informed users.

The generated network is created by a process of "preferential linking", in which new links are formed between network members with a preferability to make a connection to the more popular existing members. This process is an elaboration on the fundamental preferential attachment model.

The process of information diffusion is a variation on the SIR (susceptible-infected-recovered) model used in epidemiology. A viral hoax in the Twitter network is modelled similarly to a virus in a population.

## HOW IT WORKS

The network generation procedure: 

The model starts with two users connected by a link. At each time step, a new user is added to the network. At the same time, a certain number of new in-links are randomly attached to users in the network, based on their attractiveness, which is an initial value given to all users plus the number of in-links the users has. This continues until the network reaches a certain size of users. The current settings on the model are chosen to most accurately simulate Twitter's network.

The misinformation spreading behaviour:

The number of malicious hoax spreaders can be placed manually. These users will attempt to spread the hoax to other users. 
Each user exists in one of three states:
 Believer: a agent is in a state where they believe the false information.
 Corrector: agent i is in a state where they don’t believe false information.
 Susceptible: agent i is in a state where they are neutral to the information.

At each time step, 3 phenomena can happen to users in the network based on a probability of the users around them:

Spreading: Every susceptible user can change the state it is in to either become a believer or a corrector depending on the states of the other agents that are its neighbors. This uses the Linear Threshold model, with the weight of the neighbours having to pass a threshold in order for the agent to change its state.

Verifying: Every believer can fact-check the misinformation themselves and become a corrector based on a fixed probability.

Forgetting: Every believer or corrector can forget the information entirely and
become neutral again based on a fixed probability pf orget.

Additionally, moderation can be toggled which at each time step looks for hoax spreaders in the network and removes them.

User attitudes can be toggled as well which assigns each user a certain attitude to the misinformation which could allow them to be more easily turned into a believer of the misinformation. The distribution of user attitudes was modelled on the distribution of Twitter user's attitudes towards vaccinations in the UK.

## HOW TO USE IT

Generating the Network:

SETUP is used to place the initial nodes in the network. 

Pressing the GO ONCE button adds one new node.  To continuously add nodes, press GO. The network's current size to stop growing is 1000 nodes.

The LAYOUT? switch controls whether or not the layout procedure is run.  This procedure attempts to move the nodes around to make the structure of the network easier to see.

The PLOT? switch turns off the plots which speeds up the model.

Spreading a hoax:

The credibility parameter affects how credibile the hoax is to users in the network. A high credibility will spread the hoax more easily.

The pForgetting parameter affects how easily users in the network forget the hoax.

plant spreader changes a user in the network to a spreading state. There must be at least one initial spreader for the hoax to spread.

popularity-of-spreader allows what sort of user will be chosen to be a spreader when one is planted.

The moderation toggle allows moderation to be enabled.

The attitudes toggle makes each user attitude affect the spread of the hoax.

start-spreading is used to allow time to pass and see the spread of the hoax over time.

## THINGS TO NOTICE

The networks that result from running this model are often called "scale-free" or "power law" networks. These are networks in which the distribution of the number of connections of each node is not a normal distribution --- instead it follows what is a called a power law distribution.  Power law distributions are different from normal distributions in that they do not have a peak at the average, and they are more likely to contain extreme values (see Albert & Barabási 2002 for a further description of the frequency and significance of scale-free networks).  Barabási and Albert originally described this mechanism for creating networks, but there are other mechanisms of creating scale-free networks and so the networks created by the mechanism implemented in this model are referred to as Barabási scale-free networks.

You can see the degree distribution of the network in this model by looking at the plots. The top plot is a histogram of the degree of each node.  The bottom plot shows the same data, but both axes are on a logarithmic scale.  When degree distribution follows a power law, it appears as a straight line on the log-log plot.  One simple way to think about power laws is that if there is one node with a degree distribution of 1000, then there will be ten nodes with a degree distribution of 100, and 100 nodes with a degree distribution of 10.


## CREDITS AND REFERENCES

The network model is based on:
S. Dorogovtsev, J. F. Mendes, and A. Samukhin. Structure of growing networks with preferential linking. Physical Review Letters, 85:4633–6, 11 2000.

The spreading model is based on:
M. Tambuscio, G. Ruffo, A. Flammini, and F. Menczer. Fact-checking effect on viral hoaxes: A model of misinformation spread in social networks. 05 2015
